# Generated by Django 2.0.4 on 2018-04-16 22:48

from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('cover', models.ImageField(blank=True, null=True, upload_to='books/covers', verbose_name='Cover image')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='Name')),
                ('isbn', models.CharField(db_index=True, max_length=32, verbose_name='ISBN')),
                ('edition', models.CharField(blank=True, max_length=64, null=True, verbose_name='Edition')),
                ('published_at', models.DateTimeField(blank=True, null=True, verbose_name='Published at')),
                ('stock', models.IntegerField(default=1, verbose_name='Stock')),
                ('borrowing_price', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Borrowing price')),
            ],
            options={
                'verbose_name': 'Book',
                'verbose_name_plural': 'Books',
                'ordering': ['name'],
                'default_related_name': 'books',
            },
        ),
        migrations.CreateModel(
            name='BookAuthor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=64, verbose_name='Name')),
                ('bio', models.TextField(verbose_name='Author Biography')),
            ],
            options={
                'verbose_name': 'Book Author',
                'verbose_name_plural': 'Book Authors',
                'ordering': ['name'],
                'default_related_name': 'book_authors',
            },
        ),
        migrations.CreateModel(
            name='BookCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=64, verbose_name='Category Name')),
                ('description', models.TextField(verbose_name='Category Description')),
            ],
            options={
                'verbose_name': 'Book Category',
                'verbose_name_plural': 'Book Categories',
                'ordering': ['name'],
                'default_related_name': 'book_categories',
            },
        ),
        migrations.CreateModel(
            name='BookCollection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=64, verbose_name='Collection Name')),
                ('description', models.TextField(verbose_name='Collection Description')),
            ],
            options={
                'verbose_name': 'Book Collection',
                'verbose_name_plural': 'Book Collections',
                'ordering': ['name'],
                'default_related_name': 'book_collections',
            },
        ),
        migrations.CreateModel(
            name='BookGenre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=64, verbose_name='Genre Name')),
                ('description', models.TextField(verbose_name='Genre Description')),
            ],
            options={
                'verbose_name': 'Book Genre',
                'verbose_name_plural': 'Book Genres',
                'ordering': ['name'],
                'default_related_name': 'book_genres',
            },
        ),
        migrations.AddField(
            model_name='book',
            name='authors',
            field=models.ManyToManyField(related_name='books', to='books.BookAuthor', verbose_name='Authors'),
        ),
        migrations.AddField(
            model_name='book',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='books', to='books.BookCategory', verbose_name='Book Category'),
        ),
        migrations.AddField(
            model_name='book',
            name='collections',
            field=models.ManyToManyField(blank=True, related_name='books', to='books.BookCollection', verbose_name='Collections'),
        ),
        migrations.AddField(
            model_name='book',
            name='genres',
            field=models.ManyToManyField(blank=True, related_name='books', to='books.BookGenre', verbose_name='Genres'),
        ),
    ]
